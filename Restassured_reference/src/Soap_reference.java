import io.restassured.RestAssured;

import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Soap_reference {

	public static void main(String[] args) {

		// step1 - declare base url

		RestAssured.baseURI = "https://www.dataaccess.com";

		// step2 - declare request body

		String requestBody = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
				+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n" + "  <soap:Body>\r\n"
				+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "      <ubiNum>500</ubiNum>\r\n" + "    </NumberToWords>\r\n" + "  </soap:Body>\r\n"
				+ "</soap:Envelope>";

		// step3 - trigger the api and fetch the response body

		String responseBody = given().header("Content-Type", "text/xml; charset=utf-8").body(requestBody).when()
				.post("/webservicesserver/NumberConversion.wso").then().extract().response().getBody().asString();
		
		//step4 - print the responsebody

		System.out.println(responseBody);
		
		//step5 - extract the requestbody parameter
		
		XmlPath Xml_res=new XmlPath(responseBody);
    	String res_tag=Xml_res.getString("NumberToWordsResult");
    	System.out.println(res_tag);
    	
    	//step6 - validate the responsebody
    	
    	Assert.assertEquals(res_tag , "five hundred ");
		
	}

}
