package API_comman_methods;

import static io.restassured.RestAssured.given;

public class comman_method_handle_api {

	/// post///

	
	  public static int Post_statusCode (String requestBody, String endpoint) {
	  
	  int statuscode = given().header("Content-Type",
	  "application/json").body(requestBody).when()
	  .post(endpoint).then().extract().statusCode(); return statuscode;
	  
	  }
	  
	  public static String post_responseBody(String requestBody, String endpoint) {
	  
	  String responseBody = given().header("Content-Type",
	  "application/json").body(requestBody).when()
	  .post(endpoint).then().extract().response().asString(); return responseBody;
	  }

	/// put///

	
	 public static int Put_statusCode (String requestBody, String endpoint) {
	  
	  int statuscode = given().header("Content-Type",
	  "application/json").body(requestBody).when()
	  .put(endpoint).then().extract().statusCode(); return statuscode;
	  
	  }
	  
	  public static String put_responseBody(String requestBody, String endpoint) {
	  
	  String responseBody = given().header("Content-Type",
	  "application/json").body(requestBody).when()
	  .put(endpoint).then().extract().response().asString(); return responseBody;
	  }

	/// patch///

	
	  public static int Patch_statusCode (String requestBody, String endpoint) {
	  
	  int statuscode = given().header("Content-Type",
	  "application/json").body(requestBody).when()
	  .patch(endpoint).then().extract().statusCode(); return statuscode;
	  
	  }
	  
	  public static String patch_responseBody(String requestBody, String endpoint)
	  {
	  
	  String responseBody = given().header("Content-Type",
	  "application/json").body(requestBody).when()
	  .patch(endpoint).then().extract().response().asString(); return responseBody;
	  }

	/// get///

	public static int Get_statusCode(String endpoint) {

		int statuscode = given().when().get(endpoint).then().extract().statusCode();
		return statuscode;

	}

	public static String get_responseBody(String endpoint) {

		String responseBody = given().when().get(endpoint).then().extract().response().asString();
		return responseBody;

	}
}
